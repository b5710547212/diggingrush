var GameLayer = cc.LayerColor.extend({

    init: function() {

        this._super( new cc.Color( 225, 225, 225, 225 ) );
        this.setPosition( new cc.Point( 0, 0 ) );
         
        this.state = GameLayer.STATES.FRONT;

        this.Ymove = 1;

        this.initBackground();
        this.initPlayer();
        this.initMapArray();
        this.initHpBar();
        this.initScoreLabel();
        this.initBound();

        this.score = 0;

        this.addKeyboardHandlers();
        this.scheduleUpdate();

        return true;
                
    },

    // initBackgroundInitial: function() {

    //     this.bgInitial = new cc.Sprite('res/images/bgInit.png');
    //     this.bgInitial.setPosition( new cc.Point( screenWidth/2, screenHeight/2 - 200 ) );
    //     this.addChild( this.bgInitial );
    //     this.bgInitial.scheduleUpdate();

    // },

    initBackground: function() {

        this.bg = new cc.Sprite('res/images/bgInit.png');
        this.bg.setPosition( new cc.Point( screenWidth/2, screenHeight/2 ) );
        this.addChild( this.bg );
        this.bg.scheduleUpdate();

    },

    initPlayer: function() {

        this.player = new Player();
        this.player.setPosition( new cc.Point( screenWidth/2 , screenHeight/2 ) );
        this.addChild( this.player );
        this.player.scheduleUpdate();

    },

    initHpBar: function() {

        this.hp = new Hp();
        this.hp.setPosition( 50, 50 );
        this.hp.setScaleX( this.player.hp );
        this.addChild( this.hp );
        this.hp.scheduleUpdate();

    },

    initMapArray: function() {

        this.mapArray = [
            this.initMapLeft(),
            this.initMapMiddle1(),
            this.initMapMiddle2(),
            this.initMapRight()
        ];

    },

    initMapLeft: function() {

        this.mapLeft = new MapLeft();
        this.addChild( this.mapLeft );
        this.mapLeft.scheduleUpdate();

    },

    initMapMiddle1: function() {

        this.mapMiddle1 = new MapMiddle();
        this.addChild( this.mapMiddle1 );
        this.mapMiddle1.setPosition( new cc.Point( 20, -280 ) );
        this.mapMiddle1.scheduleUpdate();

    },

    initMapRight: function() {

        this.mapRight = new MapRight();
        this.addChild( this.mapRight );
        this.mapRight.setPosition( new cc.Point( 20, -560 ) );
        this.mapRight.scheduleUpdate();

    },

    initMapMiddle2: function() {

        this.mapMiddle2 = new MapMiddle();
        this.addChild( this.mapMiddle2 );
        this.mapMiddle2.setPosition( new cc.Point( 20, -840 ) );
        this.mapMiddle2.scheduleUpdate();

    },

    initBound: function() {

        this.bound11 = [ 60, 140 ];
        this.bound12 = [ 500, 180 ];
        this.bound1 = [ this.bound11, this.bound12 ];
        this.bound21 = [ 180, -140 ];
        this.bound22 = [ 620, -100 ];
        this.bound2 = [ this.bound21, this.bound22 ];
        this.bound31 = [ 300, -420 ];
        this.bound32 = [ 740, -380 ];
        this.bound3 = [ this.bound31, this.bound32 ];
        this.bound41 = [ 180, -700 ];
        this.bound42 = [ 620, -660 ];
        this.bound4 = [ this.bound41, this.bound42 ];
        this.bound = [ 
            this.bound1,
            this.bound2,
            this.bound3,
            this.bound4
            ];

    },

    initScoreLabel: function() {

        this.scoreLabel = cc.LabelTTF.create( '0', 'Arial', 32 );
        this.scoreLabel.setPosition( cc.p( 15 * 40, 14 * 40 + 15 ) );
        this.addChild( this.scoreLabel );

    },

    updateScoreLabel: function() {
                
        this.scoreLabel.setString( this.score );

    },

    addKeyboardHandlers: function() {

        var self = this;

        cc.eventManager.addListener({
            event: cc.EventListener.KEYBOARD,
            onKeyPressed : function( keyCode, event ) {
                self.onKeyDown( keyCode, event );
            },
            onKeyReleased: function( keyCode, event ) {
                self.onKeyUp( keyCode, event );
            }
        }, this);

    },

    onKeyDown: function( keyCode, event ) {

        if ( keyCode == cc.KEY.up ) {

            this.move( GameLayer.DIR.UP );
                    
        } else if ( keyCode == cc.KEY.down ) {

            if ( this.player.started ) {
                this.move( GameLayer.DIR.DOWN );
            } else {
                this.state = GameLayer.STATES.STARTED;
                this.player.start();
                this.startMap();
            }

        } else if ( keyCode == cc.KEY.left ) {

            this.move( GameLayer.DIR.LEFT );

        } else if ( keyCode == cc.KEY.right ) {

            this.move( GameLayer.DIR.RIGHT ); 

        }

    },

     onKeyUp: function( keyCode, event ){

        console.log( keyCode.toString() );

        if ( keyCode == cc.KEY.space ) {
            console.log( this.isDead() );
            console.log( this.player.getPosition() );
            console.log( this.score );
            console.log( this.bound[ 1 ][ 0 ][ 0 ] );
            console.log( this.bound[ 1 ][ 1 ][ 0 ] );
        }

    },

    startMap: function() {

        this.mapLeft.start();
        this.mapMiddle1.start();
        this.mapRight.start();
        this.mapMiddle2.start();

    },

    move: function( dir ) {

        this.player.move( dir );

    },

   

    update: function() {

        this.parallax();
        this.updateScoreLabel();
        this.score = this.player.score;
        if ( this.isDead() ) {
            this.dead();
        }
        this.runMap();
        if ( this.state == GameLayer.STATES.STARTED ) {
            this.updateBound();
            this.chckHit();
        }

    },

    runMap: function() {

        var lPos = this.mapLeft.getPosition();
        var m1Pos = this.mapMiddle1.getPosition();
        var rPos = this.mapRight.getPosition();
        var m2Pos = this.mapMiddle2.getPosition();

        if ( lPos.y > 720 ) this.mapLeft.setPosition( new cc.Point( 20, -400 ) );
        if ( m1Pos.y > 720 ) this.mapMiddle1.setPosition( new cc.Point( 20, -400 ) );
        if ( rPos.y > 720 ) this.mapRight.setPosition( new cc.Point( 20, -400 ) );
        if ( m2Pos.y > 720 ) this.mapMiddle2.setPosition( new cc.Point( 20, -400 ) );

    },

    updateBound: function() {

        for ( var i = 0 ; i < 4 ; i++ ) {
            for ( var j = 0 ; j < 2 ; j++ ) {
                //console.log(this.bound[i][j][1]);
                this.bound[ i ][ j ][ 1 ]++;
                if ( this.bound[ i ][ j ][ 1 ] > 720 ) {
                    this.bound[ i ][ j ][ 1 ] = -400;
                }
                //console.log(this.bound[i][j][1]);
            }
        }

    },

    isDead: function() {

        var pos = this.player.getPosition();
        return pos.y < 20 || pos.y > screenHeight - 20 || pos.x < 60 || pos.x > screenWidth - 60;

    },

    dead: function() {

        this.removeChild( this.player );

    },

    chckHit: function() {

        var pos = this.player.getPosition();
        console.log(pos.x,pos.y);
        console.log(">",this.bound[1][0][0],this.bound[1][0][1]);
        console.log(">",this.bound[1][1][0],this.bound[1][1][1]);

        if ( pos.x > this.bound[ 0 ][ 0 ][ 0 ] && pos.x < this.bound[ 0 ][ 1 ][ 0 ] && pos.y > this.bound[ 0 ][ 0 ][ 1 ] && pos.y < this.bound[ 0 ][ 1 ][ 1 ] ) this.player.hit( 1 );
        else if ( pos.x > this.bound[ 1 ][ 0 ][ 0 ] && pos.x < this.bound[ 1 ][ 1 ][ 0 ] && pos.y > this.bound[ 1 ][ 0 ][ 1 ] && pos.y < this.bound[ 1 ][ 1 ][ 1 ] ) this.player.hit( 1 );
        else if ( pos.x > this.bound[ 2 ][ 0 ][ 0 ] && pos.x < this.bound[ 2 ][ 1 ][ 0 ] && pos.y > this.bound[ 2 ][ 0 ][ 1 ] && pos.y < this.bound[ 2 ][ 1 ][ 1 ] ) this.player.hit( 1 );
        else if ( pos.x > this.bound[ 3 ][ 0 ][ 0 ] && pos.x < this.bound[ 3 ][ 1 ][ 0 ] && pos.y > this.bound[ 3 ][ 0 ][ 1 ] && pos.y < this.bound[ 3 ][ 1 ][ 1 ] ) this.player.hit( 1 );
        else this.player.unHit();

    },

    parallax: function() {

        var playerPosition = this.player.getPosition();
        this.bg.setPosition( new cc.Point( screenWidth/2, playerPosition.y/4 ) );

    }
        
});

var StartScene = cc.Scene.extend({

    onEnter: function() {
        this._super();
        var layer = new GameLayer();
        layer.init();
        this.addChild( layer );

    }
});

GameLayer.DIR = {
    UP: 1,
    DOWN: 2,
    LEFT: 3,
    RIGHT: 4
};
GameLayer.STATES = {
    FRONT: 1,
    STARTED: 2
};
