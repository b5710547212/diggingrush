var Player = cc.Sprite.extend({

	ctor: function() {

		this._super();
		this.initWithFile( 'res/images/ball.png' );

		this.started = false;

		this.xSpeed = 0;

		this.isFall = false;

		this.hitSpeed = 0;

		this.hp = 100;

		this.score = 0;
	
	},

	start: function() {

		this.started = true;
		this.isFall = true;

	},

	move: function( dir ) {

		var lastPos = this.getPosition();

		if ( dir == Player.DIR.LEFT ) this.setPosition( new cc.Point( lastPos.x - 40, lastPos.y ) );
		else if ( dir == Player.DIR.RIGHT ) this.setPosition( new cc.Point( lastPos.x + 40, lastPos.y ) );

	},

	update: function( dt ) {

		var lastPos = this.getPosition();
		var YPos = 0;

		if ( this.isFall ) {

			YPos = lastPos.y -= Player.DOWNVELOCITY;
			this.setPosition( new cc.Point( lastPos.x, YPos ) );
			Player.DOWNVELOCITY += Player.GRAVITY;

		} else {

			YPos = lastPos.y += this.hitSpeed;
			this.setPosition( new cc.Point( lastPos.x, YPos ) );

		}

	},

	hit: function( speed ) {

		this.isFall = false;
		Player.DOWNVELOCITY = 1;
		this.hitSpeed = speed;

	},

	unHit: function() {

		this.isFall = true;
		this.hitSpeed = 0;

	},

	addScore: function() {

		this.score++;

	}

});

Player.DIR = {
	UP: 1,
	DOWN: 2,
	LEFT: 3,
	RIGHT: 4
};
Player.FP = {
	Y: 0,
	X: 00
};

Player.FRICTION = -(5/4)-0.17857118;
Player.MOVINGSPEED = 10;
Player.GRAVITY = 0.5;
Player.DOWNVELOCITY = 1;