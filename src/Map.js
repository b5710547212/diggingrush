var Map = cc.Node.extend({

	ctor: function() {

		this.started = false;
		
		this._super();
		this.WIDTH = 19;
		this.HEIGHT = 5;

		this.cardMAPLEFT = [
		    'x                 x',
		    'x                 x',
		    'x###########      x',
		    'x                 x',
		    'x                 x',
			];

		this.cardMAPRIGHT = [
		    'x                 x',
		    'x                 x',
		    'x      ###########x',
		    'x                 x',
		    'x                 x',
			];

		this.cardMAPMIDLE = [
		    'x                 x',
		    'x                 x',
		    'x   ###########   x',
		    'x                 x',
		    'x                 x',
			];
		
		this.MAPLEFT = this.createMap( this.cardMAPLEFT );
		this.MAPMIDLE = this.createMap( this.cardMAPMIDLE );
		this.MAPRIGHT = this.createMap( this.cardMAPRIGHT );

	},

	createMap: function( mapCard ) {

		var temp = [];

		for ( var r = 0 ; r < this.HEIGHT ; r++ ) {

			var lineMap = [];

			for ( var c = 0; c < this.WIDTH ; c++ ) {

				var s;

				if ( mapCard[ r ][ c ] == '#' ) {

					s = cc.Sprite.create( 'res/images/SoftBlock.png' );
				   	s.setAnchorPoint( 0, 0 );
				    s.setPosition( c * 40, (this.HEIGHT - r - 1) * 40 );
				    this.addChild( s );

				} else if ( mapCard[ r ][ c ] == 'x' ) {

					s = cc.Sprite.create( 'res/images/Wall.png' );
				   	s.setAnchorPoint( 0, 0 );
				    s.setPosition( c * 40, (this.HEIGHT - r - 1) * 40 );
				    this.addChild( s );

				} else if ( mapCard[ r ][ c ] == '*' ) {

					s = cc.Sprite.create( 'res/images/ItemBlock.png' );
				   	s.setAnchorPoint( 0, 0 );
				    s.setPosition( c * 40, (this.HEIGHT - r - 1) * 40 );
				    this.addChild( s );

				} else if ( mapCard[ r ][ c ] == 'h' ) {

					s = cc.Sprite.create( 'res/images/HardBlock.png' );
					s.setAnchorPoint( 0, 0 );
				    s.setPosition( c * 40, (this.HEIGHT - r - 1) * 40 );
				    this.addChild( s );

				} lineMap[ c ] = s;
			} temp[ r ] = lineMap;
		} return temp;

	},

	start: function() {

		this.started = true;

	},

	move: function( dir ) {

		

	},

	getMap: function() {

		var temp = [ this.MAPLEFT, this.MAPMIDLE, this.MAPRIGHT ];

	},

	// findPlayerPos: function() {

	// 	var POS = [];
	// 	for( var r = 0 ; r < this.HEIGHT ; r++ ) {
	// 		for( var c = 0 ; c < this.WIDTH ; c++ ) {
	// 			if( this.MAP1[ r ][ c ] == 'o' ) {
	// 				POS[ 0 ] = r;
	// 				POS[ 1 ] = c;  
	// 			}
	// 		}
	// 	}
	// 	return POS;
	// },

	update: function( dt ) {

		

	}



});
Map.DIR = {
	UP: 1,
	DOWN: 2,
	LEFT: 3,
	RIGHT: 4
};
Map.a = 0.0005;